package numesa.carabellijuliet.perumahan.api;

import numesa.carabellijuliet.perumahan.models.DefaultResponse;
import numesa.carabellijuliet.perumahan.models.LoginResponse;
import numesa.carabellijuliet.perumahan.models.UsersResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Api {


    @FormUrlEncoded
    @POST("register")
    Call<DefaultResponse> register(
            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> login(
            @Field("emailaddress") String email,
            @Field("password") String password
    );

    @GET("allusers")
    Call<UsersResponse> getUsers();


}
